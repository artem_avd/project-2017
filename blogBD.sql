-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 22 2017 г., 20:05
-- Версия сервера: 5.5.23
-- Версия PHP: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blogcomments`
--

CREATE TABLE IF NOT EXISTS `blogcomments` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostID` int(10) unsigned NOT NULL,
  `UserID` int(11) NOT NULL,
  `CreatedON` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `Body` text NOT NULL,
  `Images` mediumblob NOT NULL,
  `enabled` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`),
  KEY `UserID_2` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Description` int(11) NOT NULL,
  `Enabled` int(11) NOT NULL,
  `CreatedOn` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(11) NOT NULL,
  `title` int(11) NOT NULL,
  `Body` int(11) NOT NULL,
  `File` blob NOT NULL,
  `UserID` int(11) NOT NULL,
  `PublishedDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `StartDate` date NOT NULL,
  `Enabled` int(11) NOT NULL,
  `CommentsEnabled` int(11) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `ShortDescription` int(11) NOT NULL,
  `CreatedOn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `blog_relationposts`
--

CREATE TABLE IF NOT EXISTS `blog_relationposts` (
  `Id` int(11) NOT NULL,
  `PostId` int(11) NOT NULL,
  `RelationPostId` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `blog_tags`
--

CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` int(11) NOT NULL,
  `PostId` int(11) NOT NULL,
  `Name` int(11) NOT NULL,
  `Enabled` int(11) NOT NULL,
  `CreatedOn` int(11) NOT NULL,
  PRIMARY KEY (`PostId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `levels`
--

CREATE TABLE IF NOT EXISTS `levels` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `CreatedOn` int(11) NOT NULL,
  `Enabled` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `CreatedOn` int(11) NOT NULL,
  `Enabled` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `Name` int(11) NOT NULL,
  `Family` int(11) NOT NULL,
  `NickName` int(11) NOT NULL,
  `CreatedOn` int(11) NOT NULL,
  `BirthDate` date NOT NULL,
  `City` int(11) NOT NULL,
  `Adress` int(11) NOT NULL,
  `ZipCode` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  `AvatarPath` mediumblob NOT NULL,
  `Email` int(11) NOT NULL,
  `Info` int(11) NOT NULL,
  `LevelId` int(11) NOT NULL,
  `Enabled` int(11) NOT NULL,
  `Password` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NickName_2` (`NickName`),
  UNIQUE KEY `NickName_3` (`NickName`),
  KEY `NickName` (`NickName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`NickName`) REFERENCES `blogcomments` (`UserID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
